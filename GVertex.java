/** CS.226.02 Project #4
 * Anand Ahuja | aahuja10
 * Jacob Lin   | ylin82
 * Ryan Dens   | rdens1
 */


/** Class to represent a GVertex (in a graph).
 *  @param <T> - data type T 
 */
public class GVertex<T extends Comparable<? super T>> 
                       implements Comparable<GVertex<T>> {

    /* Note that the nextID variable had to be moved to the graph class. */

    /** GVertex unique ID number. */
    private int num;

    /** Data stored in the GVertex. */
    private T data;

    /** Create a new GVertex.
     *  @param t the data to store in the node
     *  @param id the unique id of the node
     */
    public GVertex(T t, int id) {
        this.data = t;
        this.num = id;
    }

    /** Get the id of this GVertex.
     *  @return the id
     */
    public int id() {
        return this.num;
    }
    
    /** Set ID of the GVertex. 
     *  @param newID - newID of Vertex.
     */
    public void setID(int newID) {
        this.num = newID;
    }
    
    /** Return the data of the vertex. 
     *  @return T - data type T to return.
     */
    public T getData() {
        return this.data;
    }

    /** Get a string representation of the GVertex.
     *  @return the string 
     */
    public String toString() {
        return this.num + "";
    }

    /** Check if two vertices are the same based on ID.
     *  @param other the GVertex to compare to this
     *  @return true if the same, false otherwise
     */
    public boolean equals(Object other) {
        if (other instanceof GVertex) {
            GVertex<T> v = (GVertex<T>) other;
            return this.num == v.num;  // want these to be unique
        }
        return false;
    }

    /** Get the hashcode of a GVertex based on its ID.
     *  @return the hashcode
     */
    public int hashCode() {
        return (new Integer(this.num)).hashCode();
    }

    /** Compare two vertices based on their IDs.
     *  @param other the GVertex to compare to this
     *  @return negative if this < other, 0 if equal, positive if this > other
     */
    public int compareTo(GVertex<T> other) {
        return this.num - other.num;
    }
}
