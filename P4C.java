/** CS.226.02 Project #4
 * Anand Ahuja | aahuja10
 * Jacob Lin   | ylin82
 * Ryan Dens   | rdens1
 */


import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.util.List;

/**
 * Driver class for Part c of p4.
 * 
 * @author Ryan Dens rdens1
 * @author Anand Ahuja aahuja10
 * @author Jacob Lin ylin82
 */
public final class P4C {

    /** Private constructor for utility class. */
    private P4C() {
        System.out.println("Dont instantiate utility class");
    }

    /**
     * Convert an image to a graph of Pixels with edges between north, south,
     * east and west neighboring pixels.
     * 
     * @param image
     *            the image to convert
     * @param pd
     *            the distance object for pixels
     * @return the graph that was created
     */
    static WGraphP4<Pixel> imageToGraph(BufferedImage image, 
            Distance<Pixel> pd) {
        GVertex<Pixel> v;
        WEdge<Pixel> e;
        Pixel p;
        int id = 0; // id for vertex
        int height = image.getTileHeight(); // height in pixels
        int width = image.getTileWidth(); // width in pixels
        // GVertex<Pixel>[][] im = new GVertex[height][width];
        WGraphP4<Pixel> graph = new WGraphP4<Pixel>();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                // System.out.println("i=" + i + " j=" + j);
                p = new Pixel(image.getRGB(j, i), j, i);
                // System.out.println(image.getRGB(i, j));
                v = new GVertex<Pixel>(p, id);
                graph.addVertex(v);
                // im[i][j] = v;

                if (j != 0) {
                    // GVertex<Pixel> left = im[i][j-1];
                    GVertex<Pixel> left = graph.getMap().get(id - 1).getKey();
                    e = new WEdge<Pixel>(v, left, pd.distance(v.getData(),
                            left.getData()));
                    graph.addEdge(e);
                }
                if (i != 0) { // if not on the top
                    // if (id)
                    // GVertex<Pixel> up = im[i-1][j];
                    GVertex<Pixel> up = graph.getMap().get(id - width).getKey();
                    e = new WEdge<Pixel>(v, up, pd.distance(v.getData(),
                            up.getData()));
                    graph.addEdge(e);
                }
                id++;
            }
        }
        return graph;
    }

    /**
     * Return a list of edges in a minimum spanning forest by implementing
     * Kruskal's algorithm using fast union/finds.
     * 
     * @param g
     *            the graph to segment
     * @param kvalue
     *            the value to use for k in the merge test
     * @return a list of the edges in the minimum spanning forest
     */
    static List<WEdge<Pixel>> segmenter(WGraphP4<Pixel> g, double kvalue) {
        GraphPartition part = new GraphPartition(g, kvalue);
        PQHeap<WEdge<Pixel>> heap = new PQHeap<WEdge<Pixel>>(g.numEdges());
        ArrayList<WEdge<Pixel>> mst = new ArrayList<WEdge<Pixel>>();

        heap.init(g.allEdges());
        // System.out.println("Heap: " + heap.toString());
        while (!heap.isEmpty()) {
            /* remove min edge from heap */
            WEdge<Pixel> minEdge = heap.remove();

            /* source vertex of minEdge */
            GVertex<Pixel> sourceVertex = minEdge.source();
            /* end vertex of minEdge */
            GVertex<Pixel> endVertex = minEdge.end();
            /*
             * Union sets and add edge to mst if parent of source and end arent
             * the same.
             */
            int root1 = part.find(endVertex.id());
            int root2 = part.find(sourceVertex.id());
            if (root1 != root2) {
                boolean success = part.union(endVertex.id(), sourceVertex.id());
                if (success) {
                    mst.add(minEdge);
                }
            }
        }
        return mst;
    }

    /** Main method driving P4C.
     * @param args is the command line arguments in a string array.
     */
    public static void main(String[] args) {
        
        try {
            // the line that reads the image file
            String filename = args[0];
            BufferedImage image = ImageIO.read(new File(filename));
            int width = image.getTileWidth(); // width in pixels
            WGraphP4<Pixel> g = imageToGraph(image, new PixelDistance());
            System.out.println("width: " + width);
            List<GVertex<Pixel>> allVerts = g.allVertices();
            List<WEdge<Pixel>> res = segmenter(g, Double.parseDouble(args[1]));
            System.out.print("result =  " + res.size() + "\n");
            System.out.print("NSegments =  " + (g.numVerts() - res.size())
                    + "\n");

            // Make the segments

            WGraphP4<Pixel> newGraph = new WGraphP4<Pixel>();
            for (GVertex<Pixel> vert : allVerts) {
                newGraph.addVertex(vert);
            }
            
            for (WEdge<Pixel> e : res) {
                newGraph.addEdge(e);
            }
            
            List<GVertex<Pixel>> newVerts = newGraph.allVertices();

            HashSet<HashSet<GVertex<Pixel>>> segments = 
                    new HashSet<HashSet<GVertex<Pixel>>>();
            HashSet<GVertex<Pixel>> segHash;
            List<GVertex<Pixel>> dfs;

            for (GVertex<Pixel> vert : newVerts) {
                boolean contains = false;
                for (HashSet<GVertex<Pixel>> segment : segments) {
                    if (segment.contains(vert)) {
                        contains = true;
                        break;
                    }
                }
                if (!contains) {
                    // if it is not found then it is spanning tree add to
                    // segment
                    segHash = new HashSet<GVertex<Pixel>>();
                    dfs = newGraph.depthFirst(vert);
                    for (GVertex<Pixel> segVert : dfs) {
                        segHash.add(segVert);
                    }
                    segments.add(segHash);
                }
            }
            
            makeOutput(segments, image);
        } catch (IOException e) {
            System.out.print("Missing File!\n");

            // log the exception
            // re-throw if desired
        }
        
            
    }

   
    
    /**
     * Helper method to generate output for P4C.
     * @param segments is the hashset of the vertecies in the tree
     * @param image is the image being processed
     */
    public static void makeOutput(HashSet<HashSet<GVertex<Pixel>>> segments, 
            BufferedImage image) {
        /* the color gray */
        final int gray = 0x0DCDCDC;
        
        try {
            // After you have a spanning tree connected component x,
            // you can generate an output image like this:
            Integer numSegments = ((Integer) segments.size());
            // make a background image to put a segment into
            for (int i = 0; i < image.getHeight(); i++) {
                for (int j = 0; j < image.getWidth(); j++) {
                    image.setRGB(j, i, gray);
                }
            }

            for (HashSet<GVertex<Pixel>> x : segments) {
                for (GVertex<Pixel> i : x) {
                    Pixel d = i.getData();
                    image.setRGB(d.getX(), d.getY(), d.getColor());
                }
                String segNum = numSegments.toString();
                String outfilename = "RGBimage" + segNum + ".png";
                numSegments--;
                File f = new File(outfilename);
                ImageIO.write(image, "png", f);

                for (int i = 0; i < image.getHeight(); i++) {
                    for (int j = 0; j < image.getWidth(); j++) {
                        image.setRGB(j, i, gray);
                    }
                }

            }
            // You'll need to do that for each connected component,
            // writing each one to a different file, clearing the
            // image buffer first
        } catch (IOException e) {
            System.out.print("Missing File!\n");

            // log the exception
            // re-throw if desired
        }
    }
}
