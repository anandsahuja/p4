/** CS.226.02 Project #4
 * Anand Ahuja | aahuja10
 * Jacob Lin   | ylin82
 * Ryan Dens   | rdens1
 */


/** Public class Pixel - represents pixels. */
public class Pixel implements Comparable<Pixel> {

    /** 24 used for bit shift. */
    public final int twentyfour = 24;
    /** 16 used for bit shift. */
    public final int sixteen = 16;
    /** 8 used for bit shift. */
    public final int eight = 8;
    /** maxParse through to check if is contained by 255. */
    public final int maxParse = 0xFF;

    /** int value of pixel. */
    private int val;
    /** rgb value. */
    private int rgb;
    /** alpha value. */
    private int alpha;
    /** red value. */
    private int red;
    /** green value. */
    private int green;
    /** blue value. */
    private int blue;
    /** Position of x. */
    private int xPos;
    /** Position of y. */
    private int yPos;

    /**
     * Constructor for Pixel.
     * 
     * @param v
     *            pixel value
     * @param x
     *            Position of x
     * @param y
     *            Position of y
     */
    public Pixel(int v, int x, int y) {
        this.rgb = v;
        this.val = v;
        int firstb = (this.val >> this.twentyfour) & this.maxParse;
        this.red = (this.val >> this.sixteen) & this.maxParse;
        this.green = (this.val >> this.eight) & this.maxParse;
        this.blue = (this.val) & this.maxParse;
        this.xPos = x;
        this.yPos = y;
    }

    /**
     * Method to return color value.
     * 
     * @return rgb value
     */
    public int getColor() {
        return this.rgb;
    }

    /**
     * Method to get array of R, G, B values.
     * 
     * @return array of red, green, blue values.
     */
    public int[] getRGB() {
        int[] ret = {this.red, this.green, this.blue};
        return ret;
    }

    /**
     * Set value.
     * 
     * @param v
     *            value to set.
     */
    public void setval(int v) {

        this.val = v;
    }

    /**
     * Set X position of pixel.
     * 
     * @param x
     *            Postion of x.
     */
    public void setXPos(int x) {

        this.xPos = x;
    }

    /**
     * Set Y position of pixel.
     * 
     * @param y
     *            Position of y.
     */
    public void setYPos(int y) {

        this.yPos = y;
    }

    /**
     * Get value of pixel.
     * 
     * @return value pixel value.
     */
    public int getval() {

        return this.val;
    }

    /**
     * Get x position.
     * 
     * @return int x position
     */
    public int getX() {

        return this.xPos;
    }

    /**
     * Get y position.
     * 
     * @return int y position.
     */
    public int getY() {

        return this.yPos;
    }

    /**
     * Get the first byte.
     * 
     * @return first byte of pixel value
     */
    public int getFirstb() {
        return this.alpha;
    }

    /**
     * Get red value.
     * 
     * @return byte of red value.
     */
    public int getRed() {
        return this.red;
    }

    /**
     * Get green value in a byte.
     * 
     * @return byte of green value.
     */
    public int getGreen() {
        return this.red;
    }

    /**
     * Get blue value in a byte.
     * 
     * @return byte of blue value.
     */
    public int getBlue() {
        return this.blue;
    }

    /**
     * CompareTo method to make Pixel comparable. Although there's no need to
     * make comparable
     * 
     * @param other
     *            pixel
     * @return -1
     */
    public int compareTo(Pixel other) {
        return -1;
    }

}
