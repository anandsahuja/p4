/** CS.226.02 Project #4
 * Anand Ahuja | aahuja10
 * Jacob Lin   | ylin82
 * Ryan Dens   | rdens1
 */


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Implementation of tree-based set partitions with fast union/find. Adapted
 * from Shaffer/OpenDSA text.
 */
public class GraphPartition {

    /** RGB_SIZE = 3. */
    private static final int RGB_SIZE = 3;
    
    /** Hashmap for min/max RGB vals. */
    public HashMap<Integer, int[][]> sets;
    
    /** The size of the partition. */
    private int size;

    /** The array holding the parents for each node. */
    private int[] parent;

    /** The array holding the weights (size) of the tree for each node. */
    private int[] weight;
    
    /** The tuning parameter. */
    private double k;
    
    
    /**
     * Create a partition of singleton sets of the given size.
     * 
     * @param graph
     *            the graph to be partitioned
     * @param kValue
     *            the starting size of the partition
     */
    public GraphPartition(WGraphP4<Pixel> graph, double kValue) {
        this.sets = new HashMap<Integer, int[][]>();
        this.k = kValue;
        this.getInitSet(graph);
        this.parent = new int[graph.numVerts()];
        this.weight = new int[graph.numVerts()];
        for (int i = 0; i < graph.numVerts(); i++) {
            this.parent[i] = -1;
            this.weight[i] = 1;
        }
    }
    
    
    

    /** Get parent of node. 
     * @return the int[] representing the parent
     */
    public int[] getParent() {
        return this.parent;
    }

    
    /**
     * Initializes the partition from the graph.
     * @param graph is the graph
     */
    void getInitSet(WGraphP4<Pixel> graph) {
        // Set<Map.Entry<Integer, AbstractMap.SimpleEntry<GVertex<Pixel>,
        // ArrayList<WEdge<Pixel>>>>> verts = graph.getMap().entrySet();
        List<GVertex<Pixel>> verts = graph.allVertices();
        Iterator iter = verts.iterator();
        while (iter.hasNext()) {
            // Map.Entry<Integer, AbstractMap.SimpleEntry<GVertex<Pixel>,
            // ArrayList<WEdge<Pixel>>>> entry = (Map.Entry) iter.next();
            GVertex<Pixel> vert = (GVertex<Pixel>) iter.next();
            int[] rgb = vert.getData().getRGB();
            // System.out.println("RGBin: {" + rgb[0] + "," + rgb[1] + "," +
            // rgb[2] + "}");
            int[][] maxmin = {{rgb[0], rgb[1], rgb[2] }, 
                              {rgb[0], rgb[1], rgb[2] }};
            this.sets.put(vert.id(), maxmin);
        }
    }

    /**
     * Weighted union of the sets containing two nodes, if different.
     * 
     * @param a
     *            the first node
     * @param b
     *            the second node
     * @return
     *            true if successful, false otherwise. 
     */
    boolean union(int a, int b) {
        int root1 = this.find(a); // Find root of node a
        int root2 = this.find(b); // Find root of node b
        int[][] root1rgb = this.sets.get(root1);
        // System.out.print("Root1: {" + root1rgb[0][0] + "," + root1rgb[0][1]
        // +"," + root1rgb[0][2] + "}");
        // System.out.print("{" + root1rgb[1][0] + "," + root1rgb[1][1] +"," +
        // root1rgb[1][2] + "}");
        int[][] root2rgb = this.sets.get(root2);
        int[][] newminmax = this.calcMinMax(root1rgb, root2rgb);
        int[] diffunion = this.diff(newminmax);
        int[] diffab = this.minDiff(this.diff(root1rgb), this.diff(root2rgb));
        int mag = this.weight[root1] + this.weight[root2];
        boolean mergecond = this.calcCondition(diffunion, diffab, mag);
        if (root1 != root2 && mergecond) { // Merge with weighted union
            // count++;
            if (this.weight[root2] > this.weight[root1]) {
                this.parent[root1] = root2;
                this.weight[root2] += this.weight[root1];
                this.sets.put(root1, newminmax);
            } else {
                this.parent[root2] = root1;
                this.weight[root1] += this.weight[root2];
                this.sets.put(root2, newminmax);
            }
            return true;
        }
        return false;
    }

    /**
     * Calculates condition of the partition.
     * @param diffunion is the array representing diffunion
     * @param diffab is an int array for comparison
     * @param mag is the magnitude of the array
     * @return true if for all i < RGB_SIZE, 
     *          diffunion[i] <= diffab + k/mag
     *          else false
     */
    boolean calcCondition(int[] diffunion, int[] diffab, int mag) {
        boolean ret = true;
        for (int i = 0; i < RGB_SIZE; i++) {
            if ((double) diffunion[i] > (double) diffab[i] + this.k / mag) {
                ret = false;
            }
        }
        return ret;
    }

    /**
     * Calculates Min and Max of set1 and set2.
     * @param set1 a set being compared
     * @param set2 another seet being compared
     * @return the minmax of those sets
     */
    int[][] calcMinMax(int[][] set1, int[][] set2) {
        /*
         * System.out.print("RGB1: {" + set1[0][0] + "," + set1[0][1] +"," +
         * set1[0][2] + "}"); System.out.print("{" + set1[1][0] + "," +
         * set1[1][1] +"," + set1[1][2] + "}"); System.out.print("RGB2: {" +
         * set2[0][0] + "," + set2[0][1] +"," + set2[0][2] + "}");
         * System.out.println("{" + set2[1][0] + "," + set2[1][1] +"," +
         * set2[1][2] + "}");
         */
        int[][] ret = new int[(RGB_SIZE)  - 1][RGB_SIZE];
        for (int i = 0; i < RGB_SIZE; i++) {
            if (set1[0][i] < set2[0][i]) {
                ret[0][i] = set1[0][i];
            } else {
                ret[0][i] = set2[0][i];
            }
            if (set1[1][i] > set2[1][i]) {
                ret[1][i] = set1[0][i];
            } else {
                ret[1][i] = set2[1][i];
            }
        }
        /*
         * System.out.print("Ret: {" + ret[0][0] + "," + ret[0][1] +"," +
         * ret[0][2] + "}"); System.out.println("{" + ret[1][0] + "," +
         * ret[1][1] +"," + ret[1][2] + "}");
         */
        return ret;
    }

    /**
     * Calculates the ddiff function.
     * @param set is the 2 dimensional array being operated on
     * @return the int array representing the difference
     */
    int[] diff(int[][] set) {
        int[] ret = new int[RGB_SIZE];
        for (int i = 0; i < RGB_SIZE; i++) {
            ret[i] = set[1][i] - set[0][i];
        }
        return ret;
    }

    /**
     * Calculates minDiff on two int[].
     * @param diff1 is first int[]
     * @param diff2 is second
     * @return the minimum difference.
     */
    int[] minDiff(int[] diff1, int[] diff2) {
        int[] ret = new int[RGB_SIZE];
        for (int i = 0; i < RGB_SIZE; i++) {
            ret[i] = Math.min(diff1[i], diff2[i]);
        }
        return ret;
    }

    /**
     * Find the (root of the) set containing a node, with path compression.
     * 
     * @param curr
     *            the node to find
     * @return the root node
     */
    int find(int curr) {
        if (this.parent[curr] == -1) {
            return curr; // At root
        }
        this.parent[curr] = this.find(this.parent[curr]);
        return this.parent[curr];
    }
}
