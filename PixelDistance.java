/** CS.226.02 Project #4
 * Anand Ahuja | aahuja10
 * Jacob Lin   | ylin82
 * Ryan Dens   | rdens1
 */


/**
 * To calculate the distance between two pixel - this is defined as the sum of
 * the squared differences between each of the 4 bytes of the integer pixel
 * values. We did this implementing the Distance interface in a class called
 * PixelDistance
 * @param <T> data type T
 */

public class PixelDistance<T> implements Distance<T> {
    
    /** Default Constructor. */
    public PixelDistance() {
        
    }
    /** Method to calculate distance between two elements.
     *  @param one T one
     *  @param two T two
     *  @return double distance
     */
    public double distance(T one, T two) {
        double diff = Math.pow(((Pixel) one).getRed() 
                - ((Pixel) two).getRed(), 2);
        diff += Math.pow(((Pixel) one).getGreen() 
                - ((Pixel) two).getGreen(), 2);
        diff += Math.pow(((Pixel) one).getBlue() 
                - ((Pixel) two).getBlue(), 2);
        
        return diff;
    }
}