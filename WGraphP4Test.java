/** CS.226.02 Project #4
 * Anand Ahuja | aahuja10
 * Jacob Lin   | ylin82
 * Ryan Dens   | rdens1
 */


import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.Test;

/**
 * Test WGraphP4 using junit.
 * 
 */
public class WGraphP4Test {
    /** graph used to perform teests on. */
    static WGraphP4<String> graph;
    /** Verticies to be put into graph. */
    static GVertex<String> vertexA, vertexB, vertexC, vertexD, vertexE;
    /** Edge to be put into graph. */
    static WEdge<String> edgeA, edgeB;

    /**
     * Intitialize. runs before each test
     */
    @Before
    public void init() {
        graph = new WGraphP4<String>();

        /* Set vertecies a-e with id's 0 - 4 */
        vertexA = new GVertex<String>("a", 0);
        vertexB = new GVertex<String>("b", 1);
        vertexC = new GVertex<String>("c", 2);
        vertexD = new GVertex<String>("d", 3);
        vertexE = new GVertex<String>("e", 4);

        /* Set edges A and B with weight 1 */
        edgeA = new WEdge<String>(vertexA, vertexB, 1);
        edgeB = new WEdge<String>(vertexA, vertexC, 1);
    }

    /** Basic tests on an empty graph. */
    @Test
    public void testEmptyGraph() {
        assertEquals(graph.allVertices().size(), 0);
        assertEquals(graph.depthFirst(vertexA).size(), 0);
        assertEquals(graph.numVerts(), 0);
        assertEquals(graph.numEdges(), 0);
        assertEquals(graph.allEdges().size(), 0);

        assertFalse(graph.areAdjacent(vertexA, vertexB));
        assertEquals(graph.degree(vertexA), -1);
        assertFalse(graph.deleteEdge(vertexA, vertexB));

        assertEquals(graph.incidentEdges(vertexA).size(), 0);

        assertEquals(graph.neighbors(vertexA).size(), 0);
        assertEquals(graph.nextID(), 1);

    }

    /** Tests on a graph with no edges (completely disconnected). */
    @Test
    public void testDisconnectedGraph() {

        assertTrue(graph.addVertex(vertexA));
        assertTrue(graph.addVertex(vertexB));
        assertTrue(graph.addVertex(vertexC));
        assertTrue(graph.addVertex(vertexD));
        assertTrue(graph.addVertex(vertexE));

        assertEquals(graph.nextID(), 5);

        /* test to make sure neighbors works on disconnected graph */
        List<GVertex<String>> verts = graph.allVertices();
        for (int i = 0; i < verts.size(); i++) {
            assertEquals(graph.neighbors(verts.get(i)).size(), 0);
        }

    }

    /** Tests on an acyclic graph with depth first search. */
    @Test
    public void testDepthFirstFullAcyclic() {
        /* Setup acyclic graph */
        assertTrue(graph.addEdge(vertexA, vertexB, 1));
        assertTrue(graph.addEdge(vertexB, vertexC, 1));
        assertTrue(graph.addEdge(vertexC, vertexD, 1));
        assertTrue(graph.addEdge(vertexD, vertexE, 1));
       
        
        assertEquals(graph.numEdges(), 4);
        assertEquals(graph.allEdges().size(), 4);
        assertEquals(graph.allVertices().size(), 5);
        assertEquals(graph.numVerts(), 5);

        /* test to make sure neighbors works on connected acyclic graph */
        List<GVertex<String>> verts = graph.allVertices();
        for (int i = 0; i < verts.size(); i++) {

            if (verts.get(i).equals(vertexA) || verts.get(i).equals(vertexE)) {
                assertEquals(graph.neighbors(verts.get(i)).size(), 1);
            } else {
                assertEquals(graph.neighbors(verts.get(i)).size(), 2);
            }

            assertEquals(verts.get(i).id(), i);
        }
    }

    /** Tests on a cyclic graph with depth first search. */
    @Test
    public void testDepthFirstFullCyclic() {
        /* Setup graph with one cycle */
        assertTrue(graph.addEdge(vertexA, vertexB, 1));
        assertTrue(graph.addEdge(vertexB, vertexC, 1));
        assertTrue(graph.addEdge(vertexC, vertexD, 1));
        assertTrue(graph.addEdge(vertexD, vertexE, 1));
        assertTrue(graph.addEdge(vertexC, vertexE, 1));

        assertEquals(graph.numVerts(), 5);
        assertEquals(graph.allVertices().size(), 5);
        assertEquals(graph.numEdges(), 5);
        assertEquals(graph.allEdges().size(), 5);

        assertTrue(graph.depthFirst(vertexA).toString()
                .equals("[0, 1, 2, 4, 3]"));

        // add another cycle
        assertTrue(graph.addEdge(vertexB, vertexE, 1));
        assertEquals(graph.numVerts(), 5);
        assertEquals(graph.allVertices().size(), 5);
        assertEquals(graph.numEdges(), 6);
        assertEquals(graph.allEdges().size(), 6);
        assertTrue(graph.depthFirst(vertexA).toString()
                .equals("[0, 1, 4, 3, 2]"));

        // add leaf
        GVertex<String> vertexF = new GVertex<String>("f", 5);
        assertTrue(graph.addEdge(vertexE, vertexF, 1));
        assertFalse(graph.addVertex(vertexF));
        assertEquals(graph.numVerts(), 6);
        assertEquals(graph.allVertices().size(), 6);
        assertEquals(graph.numEdges(), 7);
        assertEquals(graph.allEdges().size(), 7);
        assertTrue(graph.depthFirst(vertexA).toString()
                .equals("[0, 1, 4, 5, 3, 2]"));

        /* test to make sure neighbors works on connected cyclic graph */
        List<GVertex<String>> verts = graph.allVertices();
        for (int i = 0; i < verts.size(); i++) {

            if (verts.get(i).equals(vertexE)) {
                assertEquals(graph.neighbors(verts.get(i)).size(), 4);
            } else if (verts.get(i).equals(vertexC)
                    || verts.get(i).equals(vertexB)) {
                assertEquals(graph.neighbors(verts.get(i)).size(), 3);
            } else if (verts.get(i).equals(vertexD)) {
                assertEquals(graph.neighbors(verts.get(i)).size(), 2);
            } else {
                assertEquals(graph.neighbors(verts.get(i)).size(), 1);
            }
        }

    }

    /** Test on a complete graph.
     * Where every vertex is connected to every other vertex by an edge.
     */
    @Test
    public void testComleteGraph() {
        /* Make complete graph */
        assertTrue(graph.addEdge(vertexA, vertexB, 1));
        assertTrue(graph.addEdge(vertexA, vertexC, 1));
        assertTrue(graph.addEdge(vertexA, vertexD, 1));
        assertTrue(graph.addEdge(vertexA, vertexE, 1));

        assertTrue(graph.addEdge(vertexB, vertexC, 1));
        assertTrue(graph.addEdge(vertexB, vertexD, 1));
        assertTrue(graph.addEdge(vertexB, vertexE, 1));

        assertTrue(graph.addEdge(vertexC, vertexD, 1));
        assertTrue(graph.addEdge(vertexC, vertexE, 1));

        assertTrue(graph.addEdge(vertexD, vertexE, 1));

        assertEquals(graph.numVerts(), 5);
        assertEquals(graph.allVertices().size(), 5);
        assertEquals(graph.numEdges(), 10);
        assertEquals(graph.allEdges().size(), 10);
        assertTrue(graph.depthFirst(vertexA).toString()
                .equals("[0, 4, 3, 2, 1]"));
    }

    /** Test addVertex method. */
    @Test
    public void testAddVertex() {
        assertTrue(graph.addVertex(vertexA));
        assertTrue(graph.allVertices().contains(vertexA));
        assertFalse(graph.addVertex(vertexA));

        assertTrue(graph.addVertex(vertexB));
        assertTrue(graph.allVertices().contains(vertexB));
    }
    
    /** Test addEdge method. */
    @Test
    public void testAddEdge() {
        assertTrue(graph.addEdge(edgeA));
        assertTrue(graph.addEdge(vertexA, vertexC, 1));
        assertFalse(graph.addEdge(vertexA, vertexB, 1));
        assertFalse(graph.addEdge(edgeB));

    }

    /** Test the areAdjacent method which returns a bool.
     * Bool says whether to verts are adjacent.
     */
    @Test
    public void testAdjacent() {
        assertTrue(graph.addVertex(vertexA));
        assertTrue(graph.addVertex(vertexB));
        assertTrue(graph.addVertex(vertexC));
        assertTrue(graph.addVertex(vertexD));
        assertTrue(graph.addVertex(vertexE));

        assertTrue(graph.addEdge(edgeA)); /* Edge from A to B */
        assertTrue(graph.addEdge(edgeB)); /* Edge from A to C */

        assertTrue(graph.areAdjacent(vertexA, vertexB));
        assertTrue(graph.areAdjacent(vertexB, vertexA));
        assertTrue(graph.areAdjacent(vertexA, vertexC));
        assertTrue(graph.areAdjacent(vertexC, vertexA));
        assertFalse(graph.areAdjacent(vertexC, vertexB));
        assertFalse(graph.areAdjacent(vertexA, vertexD));
        assertFalse(graph.areAdjacent(vertexD, vertexE));
        assertTrue(graph.neighbors(vertexA).toString().equals("[1, 2]"));
        assertTrue(graph.neighbors(vertexB).toString().equals("[0]"));
        assertTrue(graph.neighbors(vertexC).toString().equals("[0]"));
    }

    /** Tests areIncident method of a given vertex and edge. */
    @Test
    public void testIncident() {
        assertTrue(graph.addVertex(vertexA));
        assertTrue(graph.addVertex(vertexB));
        assertTrue(graph.addVertex(vertexC));

        assertTrue(graph.addEdge(edgeA));
        assertTrue(graph.areIncident(edgeA, vertexA));
        assertTrue(graph.areIncident(edgeA, vertexB));
        assertFalse(graph.areIncident(edgeA, vertexC));

        assertTrue(graph.addEdge(edgeB));
        assertTrue(graph.areIncident(edgeB, vertexA));
        assertTrue(graph.areIncident(edgeB, vertexC));
        assertFalse(graph.areIncident(edgeB, vertexB));

        assertEquals(graph.numVerts(), 3);
        assertEquals(graph.numEdges(), 2);

        assertTrue(graph.incidentEdges(vertexA).toString()
                .equals("[(0, 1, 1.0), (0, 2, 1.0)]"));
        assertTrue(graph.incidentEdges(vertexB).toString()
                .equals("[(0, 1, 1.0)]"));
        assertTrue(graph.incidentEdges(vertexC).toString()
                .equals("[(0, 2, 1.0)]"));
    }

    /** Tests the degree function of a vertex. */
    @Test
    public void testDegree() {
        assertEquals(-1, graph.degree(vertexA));

        graph.addVertex(vertexA);
        graph.addVertex(vertexB);
        graph.addVertex(vertexC);

        assertEquals(0, graph.degree(vertexA));

        graph.addEdge(edgeA);
        assertEquals(1, graph.degree(vertexA));
        assertEquals(1, graph.degree(vertexB));

        graph.addEdge(edgeB);
        assertEquals(2, graph.degree(vertexA));
        assertEquals(1, graph.degree(vertexC));
    }

    /** Tests the neighbors functon of a vertex.
     * neighbors returns an arraylist of the neighbors of the vertex
     */
    @Test
    public void testNeighbors() {
        graph.addVertex(vertexA);
        graph.addVertex(vertexB);
        graph.addVertex(vertexC);
        graph.addVertex(vertexD);

        assertTrue(graph.neighbors(vertexA).toString().equals("[]"));

        graph.addEdge(edgeA);
        assertTrue(graph.neighbors(vertexA).toString().equals("[1]"));
        assertTrue(graph.neighbors(vertexB).toString().equals("[0]"));

        graph.addEdge(edgeB);
        assertTrue(graph.neighbors(vertexA).toString().equals("[1, 2]"));
        assertTrue(graph.neighbors(vertexC).toString().equals("[0]"));

        assertTrue(graph.neighbors(vertexD).toString().equals("[]"));
    }

    /** Tests the deleteEdge function between two verts. */
    @Test
    public void testDeleteEdge() {
        graph.addVertex(vertexA);
        graph.addVertex(vertexB);
        graph.addVertex(vertexC);
        graph.addVertex(vertexD);

        assertFalse(graph.deleteEdge(vertexA, vertexB));

        graph.addEdge(edgeA);
        assertTrue(graph.allEdges().contains(edgeA));
        assertTrue(graph.deleteEdge(vertexA, vertexB));
        assertEquals(0, graph.numEdges());
        assertEquals(4, graph.numVerts());

        assertTrue(graph.addEdge(edgeA));
        assertTrue(graph.addEdge(edgeB));
        assertTrue(graph.allEdges().contains(edgeA)
                && graph.allEdges().contains(edgeB));
        assertEquals(2, graph.neighbors(vertexA).size());
        assertTrue(graph.deleteEdge(vertexA, vertexB));
        assertEquals(1, graph.neighbors(vertexA).size());
        assertEquals(0, graph.neighbors(vertexB).size());
        assertEquals(1, graph.neighbors(vertexC).size());
        assertTrue(graph.deleteEdge(vertexA, vertexC));
        assertEquals(0, graph.neighbors(vertexA).size());
        assertEquals(0, graph.neighbors(vertexB).size());
        assertEquals(0, graph.neighbors(vertexC).size());

        assertEquals(0, graph.numEdges());
    }

    /** Tests incident Edges function on a vertex.
     * returns a list of all edges incident to supplied vert.
     */
    @Test
    public void testIncidentEdges() {
        graph.addVertex(vertexA);
        graph.addVertex(vertexB);
        graph.addVertex(vertexC);

        graph.addEdge(edgeA);
        graph.addEdge(edgeB);

        assertTrue(graph.incidentEdges(vertexA).toString()
                .equals("[(0, 1, 1.0), (0, 2, 1.0)]"));
        assertTrue(graph.incidentEdges(vertexB).toString()
                .equals("[(0, 1, 1.0)]"));
        assertTrue(graph.incidentEdges(vertexC).toString()
                .equals("[(0, 2, 1.0)]"));
    }

    /** Tests the allEdges function.
     * which returns a list of all the edges in the graph */
    @Test
    public void testAllEdges() {
        graph.addVertex(vertexA);
        graph.addVertex(vertexB);
        graph.addVertex(vertexC);
        graph.addVertex(vertexD);
        graph.addEdge(edgeA);
        graph.addEdge(edgeB);
        // System.out.println(graph.allEdges().toString());
        assertEquals("[(0, 1, 1.0), (0, 2, 1.0)]", graph.allEdges().toString());
    }

    /** Tests the allEdges function.
     * whih returns a list of all verts in the graph
     */
    @Test
    public void testAllVertices() {
        graph.addVertex(vertexA);
        graph.addVertex(vertexB);
        graph.addVertex(vertexC);
        graph.addVertex(vertexD);
        graph.addVertex(vertexE);
        assertTrue(graph.allVertices().toString().equals("[0, 1, 2, 3, 4]"));
    }

    /** Tests numEdges function which returns number of edges in graph. */
    @Test
    public void testNumEdges() {
        graph.addEdge(edgeA);
        assertTrue(graph.numEdges() == 1);
        graph.addEdge(vertexA, vertexC, 1);
        assertTrue(graph.numEdges() == 2);
        assertTrue(graph.deleteEdge(vertexA, vertexB));
        assertTrue(graph.numEdges() == 1);
        assertTrue(graph.deleteEdge(vertexA, vertexC));
        assertTrue(graph.numEdges() == 0);
    }

    /** Tests numVets function which returns number of verts in graph. */ 
    @Test
    public void testNumVerts() {
        assertTrue(graph.numVerts() == 0);
        assertTrue(graph.addVertex(vertexA));
        assertTrue(graph.numVerts() == 1);
        assertTrue(graph.addVertex(vertexB));
        assertTrue(graph.numVerts() == 2);
        assertFalse(graph.addVertex(vertexB));
        assertTrue(graph.numVerts() == 2);
    }

    /** Tests kruskals algorithm on graph.
     * Which returns the list of edges in the Min Spanning tree of graph.
     */
    @Test
    public void testKruskals() {
        WGraphP4<String> newGraph = new WGraphP4<String>();

        GVertex<String> a = new GVertex<String>("a", 0);
        GVertex<String> b = new GVertex<String>("b", 1);
        GVertex<String> c = new GVertex<String>("c", 2);
        GVertex<String> d = new GVertex<String>("d", 3);
        GVertex<String> e = new GVertex<String>("e", 4);

        WEdge<String> edge1 = new WEdge<String>(a, b, 10);
        WEdge<String> edge2 = new WEdge<String>(c, b, 9);
        WEdge<String> edge3 = new WEdge<String>(c, d, 2);
        WEdge<String> edge4 = new WEdge<String>(d, b, 12);
        WEdge<String> edge5 = new WEdge<String>(d, e, 7);
        WEdge<String> edge6 = new WEdge<String>(a, d, 5);
        WEdge<String> edge7 = new WEdge<String>(a, e, 3);

        newGraph.addEdge(edge1);
        newGraph.addEdge(edge2);
        newGraph.addEdge(edge3);
        newGraph.addEdge(edge4);
        newGraph.addEdge(edge5);
        newGraph.addEdge(edge6);
        newGraph.addEdge(edge7);

        List<WEdge<String>> testList = new ArrayList<WEdge<String>>();
        testList.add(edge3);
        testList.add(edge7);
        testList.add(edge6);
        testList.add(edge2);

        List<WEdge<String>> kruskalsOutput = newGraph.kruskals();
        // System.out.println(kruskalsOutput.toString());
        for (int j = 0; j < kruskalsOutput.size(); j++) {
            assertEquals(kruskalsOutput.get(j), testList.get(j));
        }

    }

}