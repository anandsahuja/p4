/* Anand Ahuja | aahuja10@jhu.edu.
 * CS.600.226.03 - Assignment 4
 * Tests for a PQHeap implementation of MaxPriorityQueue
 *      - exclusively tests methods defined in interface MaxPriorityQueue
 */


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Test for a min PQHeap.
 */
public class PQHeapTest {
    
    /** PQHeap 1, used for testing. */
    static PQHeap<Integer> pq1;
    /** ArrayList used for testing. */
    static ArrayList<Integer> iray = new ArrayList<Integer>();
    /** PQHeap init. */
    static PQHeap<Integer> initpq;
    /** PQHeap 2. */
    static PQHeap<String> pq2;
    
    
    
    /** Set up before class starts. */
    @BeforeClass
    public static void setUpBeforeClass() {
        pq1 = new PQHeap<Integer>(11);
        assertTrue(pq1.isEmpty());
        assertEquals(pq1.size(), 0);
        initpq = new PQHeap<Integer>(12);
        assertTrue(initpq.isEmpty());
        assertEquals(initpq.size(), 0);
        pq2 = new PQHeap<String>(11);
        assertTrue(pq2.isEmpty());
        assertEquals(pq2.size(), 0);
        for (int i = 0; i <= 10; i++) {
            iray.add(i);
        }
    }
    
    
    
    /** Set up before every test. */
    @Before
    public void setUp() {
        pq1.clear();
        assertTrue(pq1.isEmpty());
        assertEquals(pq1.size(), 0);
        for (int i = 0; i < iray.size(); i++) {
            pq1.insert(iray.get(i));
        }
        assertEquals(pq1.size(), iray.size());
        assertFalse(pq1.isEmpty());
        
        initpq.clear();
        assertTrue(initpq.isEmpty());
        assertEquals(initpq.size(), 0);
        initpq.init(iray);
        assertEquals(initpq.size(), iray.size());
        assertFalse(initpq.isEmpty());
        
        pq2.clear();
        
        assertTrue(pq2.isEmpty());
        assertEquals(pq2.size(), 0);  
    }
    
    
    
    
    
    
    /** Test case to test clear empty PQHeap. */
    @Test
    public void testClearEmpty() {
        assertTrue(pq2.isEmpty());
        assertEquals(pq2.size(), 0);
        pq2.clear();        
        assertTrue(pq2.isEmpty());
        assertEquals(pq2.size(), 0);
    }
    
    
   
    /** Test case to test Insert then Clear. */
    @Test
    public void testInsertThenClear() {
        assertTrue(pq2.isEmpty());
        assertEquals(pq2.size(), 0);
        for (int i = 0; i < iray.size() - 1; i++) {
            pq2.insert(iray.get(i).toString());
            // System.out.println(pq2.toString());
        }
        pq2.insert(((Integer) 11).toString());
        // System.out.println(pq2.toString());
        assertEquals(pq2.size(), iray.size());
        assertFalse(pq2.isEmpty());
        pq2.clear();
        assertTrue(pq2.isEmpty());
        assertEquals(pq2.size(), 0);
    }
    
    
    /** Test case to test remove then clear. */
    @Test
    public void testremoveThenClear() {
        assertEquals(pq1.size(), iray.size());
        assertFalse(pq1.isEmpty());
        //System.out.println(pq1.toString());
        Integer min = pq1.remove();
        //System.out.println(pq1.toString());
        assertEquals(pq1.size(), iray.size() - 1);
        assertEquals(min, iray.get(0));
        
        min = pq1.remove();
        
        assertEquals(pq1.size(), iray.size() - 2);
        assertEquals(min, iray.get(1));
        
        pq1.clear();
        assertTrue(pq2.isEmpty());
        assertEquals(pq2.size(), 0); 
    }  
    
    
    /** Test case to test peek on empty. */
    @Test (expected = QueueEmptyException.class)
    public void testpeekOnEmpty() {
        assertEquals(pq2.size(), 0);
        assertTrue(pq2.isEmpty());
        pq2.peek();

    }
    
    /** Test case to test Peek on Full PQHeap and type. */
    @Test
    public void testpeekOnFullAndType() {
        assertEquals(pq1.size(), iray.size());
        assertFalse(pq1.isEmpty());
        Integer min = pq1.peek();
        assertEquals(min, iray.get(0));
        assertTrue(min instanceof Integer);
        
        min = pq1.peek();
        
        assertEquals(min, iray.get(0));
        assertTrue(min instanceof Integer);
    }
    
    
    
    /** Test case to test peek, no removal. */
    @Test
    public void testpeekNoRemoval() {
        assertEquals(pq1.size(), iray.size());
        assertFalse(pq1.isEmpty());
        Integer min = pq1.peek();
        assertEquals(min, iray.get(0));
        assertTrue(min instanceof Integer);
        assertEquals(pq1.size(), iray.size());
        
        min = pq1.peek();
        assertEquals(min, iray.get(0));
        assertTrue(min instanceof Integer);
        assertEquals(pq1.size(), iray.size());
        
        Integer remove = pq1.remove();
        assertEquals(min, remove);
    }

    
    /** Test case to test remove on empty PQHeap. */
    @Test (expected = QueueEmptyException.class)
    public void testremoveOnEmpty() {
        assertEquals(pq2.size(), 0);
        assertTrue(pq2.isEmpty());
        pq2.remove();
    }

    
    /** Test case on remove and see if updates size. */
    @Test
    public void testremoveUpdatesSize() {
        assertEquals(pq1.size(), iray.size());
        assertFalse(pq1.isEmpty());
        pq1.remove();
        assertEquals(pq1.size(), iray.size() - 1);
        
        pq1.remove();
        assertEquals(pq1.size(), iray.size() - 2);
    }
    
    
    /** Test case to remove the correct val and type. */
    @Test
    public void testremoveCorrectValueAndType() {
        assertEquals(pq1.size(), iray.size());
        assertFalse(pq1.isEmpty());
        Integer min = pq1.remove();
        assertEquals(min, iray.get(0));
        assertTrue(min instanceof Integer);
        Integer peek = pq1.peek();
        min = pq1.remove();
        assertEquals(min, iray.get(1));
        assertTrue(min instanceof Integer);
        assertEquals(min, peek);
    }
    
    
    /** Test case to remove until empty. */
    @Test
    public void testremoveUntilEmpty() {
        assertEquals(pq1.size(), iray.size());
        assertFalse(pq1.isEmpty());
        for (Integer i = 0; i < iray.size(); i++) {
            Integer peek = pq1.peek();
            Integer min = pq1.remove();
            Integer val = iray.get(i);
            assertEquals(peek, min);
            assertEquals(min, val);
            assertTrue(min instanceof Integer);
            assertEquals(pq1.size(), iray.size() - i - 1);
        }
        assertTrue(pq1.isEmpty());
        assertEquals(pq1.size(), 0);       
    }
    
    
    /** Test case to insert then remove. */
    @Test
    public void testInsertThenRemove() {
        // Insert
        assertTrue(pq2.isEmpty());
        assertEquals(pq2.size(), 0);
        for (int i = 0; i < iray.size(); i++) {
            pq2.insert(iray.get(i).toString());
            assertEquals(pq2.size(), i + 1);
            assertFalse(pq2.isEmpty());
        }
        //System.out.println(pq2);
        assertEquals(pq2.size(), iray.size());
        assertFalse(pq2.isEmpty());
        // Remove
        for (Integer i = 0; i < iray.size(); i++) {
            Integer peek = pq1.peek();
            Integer min = pq1.remove();
            Integer val = iray.get(i);
            assertEquals(peek, min);
            assertEquals(min, val);
            assertTrue(min instanceof Integer);
            assertEquals(pq1.size(), iray.size() - i - 1);
        }
        assertTrue(pq1.isEmpty());
        assertEquals(pq1.size(), 0);  
    }
    
    
    /** Test case on Inserting new values and size updates. */
    @Test
    public void testInsertAddNewValueUpdatesSize() {
        assertTrue(pq2.isEmpty());
        assertEquals(pq2.size(), 0);
        pq2.insert("1");
        String peek = pq2.peek();
        assertEquals(peek, "1");
        assertFalse(pq2.isEmpty());       
        assertEquals(pq2.size(), 1);
        pq2.insert("2");
        assertEquals(pq2.size(), 2);
        String min = pq2.remove();
        assertFalse(pq2.isEmpty());
        assertEquals(min, "1");
        assertEquals(pq2.size(), 1);
        min = pq2.remove();
        assertEquals(min, "2");
        assertTrue(pq2.isEmpty()); 
    }    
    
   
    /** Test to insert duplicates. */
    @Test
    public void testInsertDuplicates() {
        assertTrue(pq2.isEmpty());
        assertEquals(pq2.size(), 0);
        pq2.insert("1");
        String peek = pq2.peek();
        assertEquals(peek, "1");
        assertFalse(pq2.isEmpty());       
        assertEquals(pq2.size(), 1);
        pq2.insert("1");
        assertEquals(peek, "1");
        assertFalse(pq2.isEmpty());       
        assertEquals(pq2.size(), 2);
        String max1 = pq2.remove();
        String max2 = pq2.remove();
        assertEquals(max1, "1");
        assertEquals(max1, max2);
        assertTrue(pq2.isEmpty());     
    }
    
    
    /** Test case to test initpq cases. */
    @Test
    public void testInit() {
        assertEquals(pq1.size(), initpq.size());
        assertFalse(pq1.isEmpty());
        assertFalse(initpq.isEmpty());
        assertEquals(initpq.size(), iray.size());
        
       
        
        
        for (Integer i = 0; i < iray.size(); i++) {
            Integer val = iray.get(i);
            Integer pq1Val = pq1.remove();
            Integer initpqVal = initpq.remove();
            
            //System.out.println(val + " " + pq1Val + " " + initpqVal);
            
            assertEquals(val, pq1Val);
            assertEquals(pq1Val, initpqVal);
            assertTrue(initpqVal instanceof Integer);
            assertEquals(pq1.size(), iray.size() - i - 1);
            assertEquals(initpq.size(), iray.size() - i - 1);
            // System.out.println(initpq);
            // System.out.println(pq1);
        }
        assertTrue(pq2.isEmpty());
    }
    
}
