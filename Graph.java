/** @author Anand Ahuja, Jacob Lin, Ryan Dens 
 *  aahuja10, ylin82, rdens1
 *  Assignment 4
 *  cs226.02
 */

import java.util.List;

/** Public Interface of a Graph. */
public interface Graph {

    /** Get the number of edges. 
     *  @return int number of Edges
     */
    int numEdges();

    /** Get the number of vertices. 
     * @return int number of vertices
     */
    int numVerts();

    /** Get the next ID to use in making a vertex. 
     *  @return int next ID
     */
    int nextID();

    /** Create and add a vertex to the graph.
     *  @param d the data to store in the vertex
     *  @return true if successful, false otherwise
     */
    boolean addVertex(Object d);

    /** Add a vertex if it doesn't exist yet. 
     *  @param v Vertex to add. 
     *  @return true if successful.
     */
    boolean addVertex(Vertex v);

    /** Add an edge, may also add the incident vertices.
     *  @param e Edge to add. 
     *  @return true if successful.
     */
    boolean addEdge(Edge e);

    /** Add a (directed) edge, may also add the incident vertices.
     *  @param v Vertex 1 end
     *  @param u Vertex 2 source
     *  @return true if successful. 
     */
    boolean addEdge(Vertex v, Vertex u);

    /** Remove a (directed) edge if there.  
     *  @param v vertex 1 end
     *  @param u vertex 2 source
     *  @return true if successful
     */
    boolean deleteEdge(Vertex v, Vertex u);

    /** Return true if there is an edge between v and u. 
     *  @param v vertex 1
     *  @param u vertex 2
     *  @return true if adjacent. 
     */
    boolean areAdjacent(Vertex v, Vertex u);

    /** Return a list of all the neighbors of vertex v.  
     *  @param v Vertex to check.
     *  @return List<Vertex> of neighbors of v. 
     */
    List<Vertex> neighbors(Vertex v);

    /** Return the number of edges incident to v. 
     *  @param v - vertex to check
     *  @return int degree of vertex.
     */
    int degree(Vertex v);

    /** Return true if v is an endpoint of edge e.  
     * 
     * @param e Edge
     * @param v Vertex
     * @return true if edge and vertex are incident. 
     */
    boolean areIncident(Edge e, Vertex v);

    /** Return a list of all the vertices that can be reached from v,
     * in the order in which they would be visited in a depth-first
     * search starting at v.  
     * 
     * @param v Vertex.
     * @return List<Vertex> by depth first. 
     */
    List<Vertex> depthFirst(Vertex v);

    /** Return a list of all the edges.  
     * 
     * @return List of all Edges.
     */
    List<Edge> allEdges();

    /** Return a list of all the vertices.  
     * 
     * @return List of all Vertices.
     */
    List<Vertex> allVertices();

}
