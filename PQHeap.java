/** Anand Ahuja | aahuja10@jhu.edu.
 * CS.600.226.03 - Assignment 4
 * Stubs for a MaxPQHeap implementation of MaxPriorityQueue
 *      - exclusively implements methods defined in interface 
 *      MaxPriorityQueue
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
//import java.lang.reflect.Array;

/**
 * PQHeap class.
 * 
 * @param <T>
 *            type
 */

public class PQHeap<T extends Comparable<? super T>> 
                      implements PriorityQueue<T> {

    /** Compartor used to compare elements. */
    private Comparator<T> comparator;
    /** Array implementation of heap. */
    private T[] heap;
    /** Size of heap. */
    private int size;
    /** Maximum entries. */
    private int max;

    /** Comparator class used to compare elements. 
     * @param <T> data type T
     */
    private static class DefaultComparator<T extends Comparable<T>> 
                                            implements Comparator<T> {
        /**
         * Compare method for two data elements.
         * @param t1 - element 1
         * @param t2 - element 2 return int - comparative
         * @return int - comparative val
         */
        public int compare(T t1, T t2) {
            return t1.compareTo(t2);
        }
    }

    /**
     * Constructor with param MaxEntries.
     * 
     * @param maxEntries
     *            - maxEntries.
     */
    public PQHeap(int maxEntries) {
        this.max = maxEntries;
        this.heap = (T[]) new Comparable[this.max + 1];
        this.size = 0;
        this.comparator = null;
    }

    /**PQHeap constructor 2.
     * @param maxEntries Maximum Entries
     * @param comp Comparator
     */
    public PQHeap(int maxEntries, Comparator<T> comp) {
        this.max = maxEntries;
        this.heap = (T[]) new Comparable[this.max + 1];
        this.size = 0;
        this.comparator = comp;
    }

    /**
     * Get the number of elements in the priority queue.
     * 
     * @return the size
     */
    public int size() {
        return this.size;
    }

    /**
     * Determine if the priority queue is empty or not.
     * 
     * @return true if empty, false otherwise
     */
    public boolean isEmpty() {
        return this.size == 0;
    }

    /**
     * Dump the contents of the priority queue.
     */
    public void clear() {
        this.size = 0;
        this.heap = (T[]) new Comparable[this.max + 1];
    }

    /**
     * Remove "best" value. This value is the "best" value in the queue as
     * determined by the comparator for the queue.
     * 
     * @throws QueueEmptyException if queue is empty.
     * @return T returns minimum element in heap
     */
    public T remove() throws QueueEmptyException {
        if (this.size == 0) {
            throw new QueueEmptyException();
        }
        T best = this.heap[1];
        this.swap(1, this.size);
        this.size--;
        if (this.size != 0) {
            this.bubbleDown(1);
        }
        this.heap[this.size + 1] = null;
        return best;
    }
    
    /** Bubbling up element in tree.
     * 
     * @param pos position
     */
    public void bubbleUp(int pos) {
        while (pos != 1 && this.compare(pos, pos / 2) < 0) {
            this.swap(pos, pos / 2);
            pos /= 2;
        }
    }

    /** Bubbling down element in tree.
     * 
     * @param pos position
     */
    public void bubbleDown(int pos) {
        while (!((pos > this.size / 2) && (pos <= this.size))) {
            int child = 2 * pos;
            if (child < this.size && this.compare(child, child + 1) > 0) {
                child++;
            }
            if (this.compare(child, pos) >= 0) {
                break;
            }
            this.swap(child, pos);
            pos = child;
        }
    }

    /** Swap element based on indices.
     * 
     * @param index1 index 1
     * @param index2 index 2
     */
    public void swap(int index1, int index2) {
        T tmp = this.heap[index1];
        this.heap[index1] = this.heap[index2];
        this.heap[index2] = tmp;
    }

    /**Compare method between 2 indices.
     * 
     * @param index1 - index 1
     * @param index2 - index 2
     * @return int - comparative value
     */
    public int compare(int index1, int index2) {
        if (this.comparator == null) {
            return this.heap[index1].compareTo(this.heap[index2]);
        } else {
            return this.comparator.compare(this.heap[index1], 
                                           this.heap[index2]);
        }
    }

    /**
     * Get the "best" value. This value is the "best" value in the queue as
     * determined by the comparator for the queue. [Note, "best" is at the root
     * in a heap-based implementation.]
     * 
     * @return best value in the queue.
     * @throws QueueEmptyException
     *             If queue is empty.
     */
    public T peek() throws QueueEmptyException {
        if (this.size == 0) {
            throw new QueueEmptyException();
        } else {
            return this.heap[1];
        }
    }

    /**
     * Add a value to the priority queue.
     * 
     * @param val
     *            the value to be added (duplicates ok)
     */
    public void insert(T val) {
        if (this.size == this.max) {
            return;
        }
        int pos = this.size + 1;
        this.heap[pos] = val;
        this.bubbleUp(pos);
        this.size++;
    }

    /**
     * Initialize a priority queue from a container of values.
     * 
     * @param values
     *            the collection of starting values
     */
    public void init(Collection<T> values) {
        /*
         * for (T val : values) { this.insert(val); }
         */

        this.size = 0;

        int i = 1;
        for (T t : values) {
            this.heap[i] = t;
            this.size++;
            i++;
        }

        for (i = this.size / 2; i > 0; i--) {
            this.bubbleDown(i);
        }
    }

    /**
     * ToString method, prints readable string.
     * 
     * @return String
     */
    public String toString() {
        String ret = "[ ";
        for (T ele : this.heap) {
            ret = ret + ele + " ";
        }
        ret = ret + "]";
        return ret;
    }

    /**
     * Main Method.
     * 
     * @param args
     *            command line argument
     */
    public static void main(String[] args) {
        final int eleven = 11;
        PQHeap<Integer> a1 = new PQHeap<Integer>(eleven);
        ArrayList<Integer> iray = new ArrayList<Integer>();
        final int ten = 10;
        for (int i = 0; i <= ten; i++) {
            iray.add(i);
        }
        for (int i = 0; i < iray.size(); i++) {
            a1.insert(iray.get(i));
            System.out.println(a1.toString());
        }

    }

}
