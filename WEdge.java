/** CS.226.02 Project #4
 * Anand Ahuja | aahuja10
 * Jacob Lin   | ylin82
 * Ryan Dens   | rdens1
 */


/** Implementation of an edge class (for graphs), could be directed or not.
 *  @param <T> Data type T
 */
public class WEdge<T extends Comparable<? super T>> 
                     implements Comparable<WEdge<T>> {

    /** Starting vertex of an edge. */
    private GVertex<T> source;
    /** Ending vertex of an edge. */
    private GVertex<T> end;
    /** Whether or not the edge is directed. */
    private boolean directed;
    /** The weighting of the edge. */
    private double weight;

    /** Create an undirected edge.
     *  @param u the start
     *  @param v the end
     *  @param w weight
     */
    public WEdge(GVertex<T> u, GVertex<T> v, double w) {
        this.source = u;
        this.end = v;
        this.directed = false;
        this.weight = w;
    }

    /** Create an edge.
     *  @param u the start
     *  @param v the end
     *  @param w weight
     *  @param dir true if directed, false otherwise
     */
    public WEdge(GVertex<T> u, GVertex<T> v, double w, boolean dir) {
        this.source = u;
        this.end = v;
        this.weight = w;
        this.directed = dir;
    }

    /** Is the edge directed.
     *  @return true if yes, false otherwise
     */
    public boolean isDirected() {
        return this.directed;
    }

    /** Is a vertex incident to this edge.
     *  @param v the vertex
     *  @return true if source or end, false otherwise
     */
    public boolean isIncident(GVertex<T> v) {
        return this.source.equals(v) || this.end.equals(v);
    }

    /** Get the starting endpoint vertex.
     *  @return the vertex
     */
    public GVertex<T> source() {
        return this.source;
    }

    /** Get the ending endpoint vertex.
     *  @return the vertex
     */
    public GVertex<T> end() {
        return this.end;
    }

    /**
     * Return the weight of the edge.
     * @return the weight of the edge
     */
    public double weight() {
        return this.weight;
    }
    
    /** Create a string representadtion of the edge.
     *  @return the string as (source,end)
     */
    public String toString() {
        return "(" + this.source + ", " + this.end + ", " + this.weight + ")";
    }

    /** Check if two edges are the same.
     *  @param other the edge to compare to this
     *  @return true if directedness and endpoints match, false otherwise
     */
    @Override 
    public boolean equals(Object other) {
    
        if (other instanceof WEdge) {
            WEdge e = (WEdge) other;

            if (this.directed != e.directed) {
                return false;
            }
            if (this.directed) {
                return this.source.equals(e.source)
                    && this.end.equals(e.end);
            } else {
                return this.source.equals(e.source)
                    && this.end.equals(e.end)
                    || this.source.equals(e.end)
                    && this.end.equals(e.source);
            }
        }
        return false;
    }

    /** Make a hashCode based on the toString.
     *  @return the hashCode
     */
    public int hashCode() {
        return this.toString().hashCode();
    }
    
    /** Compare two vertices based on their IDs.
     *  @param other the GVertex to compare to this
     *  @return negative if this < other, 0 if equal, positive if this > other
     */
    public int compareTo(WEdge<T> other) {
        double ret = this.weight - other.weight;
        if (ret > 0) {
            return 1;
        } else if (ret < 0) {
            return -1;
        }
        return (int) ret; // if this == other ret will be 0        
    }

}
