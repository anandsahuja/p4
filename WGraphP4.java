/** CS.226.02 Project #4
 * Anand Ahuja | aahuja10
 * Jacob Lin   | ylin82
 * Ryan Dens   | rdens1
 */


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Iterator;
import java.util.Map;
import java.util.AbstractMap;

/**
 * Public class WGraph, an implementation of Graph specialized for this p4.
 * 
 * @param <VT>
 *            is the type in the Graph
 */
public class WGraphP4<VT extends Comparable<? super VT>> implements WGraph<VT> {

    /** Array to hold all outside graph created vertexID. */
    public HashSet<Integer> ids = new HashSet<Integer>();

    /** Graph is maintained by list of edges incident to each vertex. */
    private HashMap<Integer, AbstractMap.SimpleEntry<GVertex<VT>, 
        ArrayList<WEdge<VT>>>> incidentEdges;

    /** Counter for number of edges in graph. */
    private int numEdges;

    /** Used to sequentially generate vertex IDs for this graph! */
    private int nextID;

    /**
     * 
     */
    public WGraphP4() {
        this.nextID = 0;
        this.numEdges = 0;
        this.incidentEdges = new HashMap<Integer, 
                AbstractMap.SimpleEntry<GVertex<VT>, ArrayList<WEdge<VT>>>>();
    }

    /**
     * Get the number of edges.
     * 
     * @return the number
     */
    public int numEdges() {
        return this.numEdges;
    }

    /**
     * Get the number of vertices.
     * 
     * @return the number
     */
    public int numVerts() {
        return this.incidentEdges.size();
    }

    /**
     * Get the next ID to use in making a vertex.
     * 
     * @return the id
     */
    public int nextID() {
        int tmp = this.nextID;
        while (this.ids.contains(tmp + 1)) {
            tmp++;
        }
        tmp++;
        this.nextID = tmp;
        return this.nextID;
    }

    /**
     * Returns the hashmap of the edges in the graph.
     * 
     * @return the graph
     */
    public HashMap<Integer, AbstractMap.SimpleEntry<GVertex<VT>, 
        ArrayList<WEdge<VT>>>> getMap() {
        return this.incidentEdges;
    }

    /**
     * Create and add a vertex to the graph.
     * 
     * @param d
     *            the data to store in the vertex
     * @return true if successful, false otherwise
     */
    public boolean addVertex(VT d) {
        GVertex<VT> tmp = new GVertex<VT>(d, this.nextID());
        if (this.allVertices().contains(tmp)) {
            return false;
        } else {
            ArrayList<WEdge<VT>> newIE = new ArrayList<WEdge<VT>>();
            AbstractMap.SimpleEntry<GVertex<VT>, ArrayList<WEdge<VT>>> ent = 
                    new AbstractMap.SimpleEntry<GVertex<VT>,
                        ArrayList<WEdge<VT>>>(tmp, newIE);
            
            this.incidentEdges.put(tmp.id(), ent);
            return true;
        }
    }

    /**
     * Add a vertex if it doesn't exist yet.
     * 
     * @param tmp
     *            the vertex to add
     * @return false if already there, true if added
     */
    public boolean addVertex(GVertex<VT> tmp) {
        this.ids.add(tmp.id());
        if (this.allVertices().contains(tmp)) {
            return false;
        } else {
            ArrayList<WEdge<VT>> newIE = new ArrayList<WEdge<VT>>();
            AbstractMap.SimpleEntry<GVertex<VT>, 
                ArrayList<WEdge<VT>>> ent = new AbstractMap.SimpleEntry
                    <GVertex<VT>, ArrayList<WEdge<VT>>>(tmp, newIE);
            this.incidentEdges.put(tmp.id(), ent);
            return true;
        }
    }

    /**
     * Add a weighted edge, may also add the incident vertices.
     * 
     * @param e
     *            the edge to add
     * @return false if already there, true if added
     */
    public boolean addEdge(WEdge<VT> e) {
        GVertex<VT> end = e.end();
        GVertex<VT> source = e.source();
        AbstractMap.SimpleEntry<GVertex<VT>, 
            ArrayList<WEdge<VT>>> endEdges = this.incidentEdges.get(end.id());
        AbstractMap.SimpleEntry<GVertex<VT>, 
            ArrayList<WEdge<VT>>> sourceEdges = 
                this.incidentEdges.get(source.id());
        if (endEdges != null) {
            if (!endEdges.getValue().contains(e)) {
                ArrayList<WEdge<VT>> newSet = endEdges.getValue();
                newSet.add(e);
                endEdges.setValue(newSet);
                this.incidentEdges.put(end.id(), endEdges);
            } else {
                return false;
            }

        } else {
            this.addVertex(end);
            endEdges = this.incidentEdges.get(end.id());
            ArrayList<WEdge<VT>> newSet = endEdges.getValue();
            newSet.add(e);
            endEdges.setValue(newSet);
            this.incidentEdges.put(end.id(), endEdges);
        }

        if (sourceEdges != null) {
            if (!sourceEdges.getValue().contains(e)) {
                ArrayList<WEdge<VT>> newSet = sourceEdges.getValue();
                newSet.add(e);
                sourceEdges.setValue(newSet);
                this.incidentEdges.put(source.id(), sourceEdges);
            } else {
                return false;
            }
        } else {
            this.addVertex(source);
            sourceEdges = this.incidentEdges.get(source.id());
            ArrayList<WEdge<VT>> newSet = sourceEdges.getValue();
            newSet.add(e);
            sourceEdges.setValue(newSet);
            this.incidentEdges.put(source.id(), sourceEdges);
        }
        this.numEdges++;
        return true;
    }

    /**
     * Checks to see if an edge is containe in the arraylist.
     * @param in the arraylist of edges
     * @param e the edge to be checked for
     * @return true if the edge is there, else false.
     */
    public boolean incidentEdgeContains(ArrayList<WEdge<VT>> in, WEdge<VT> e) {
        for (WEdge<VT> ee : in) {
            if (ee.equals(e)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add a weighted edge, may also add vertices.
     * 
     * @param v
     *            the starting vertex
     * @param u
     *            the ending vertex
     * @param weight
     *            the weight of the edge
     * @return false if already there, true if added
     */
    public boolean addEdge(GVertex<VT> v, GVertex<VT> u, double weight) {
        WEdge<VT> e = new WEdge<VT>(v, u, weight);
        return this.addEdge(e);
        /*
         * GVertex<VT> end = e.end(); GVertex<VT> source = e.source();
         * ArrayList<WEdge<VT>> endEdges = incidentEdges.get(end);
         * ArrayList<WEdge<VT>> sourceEdges = incidentEdges.get(source); if
         * (endEdges != null) { if (!endEdges.contains(e)) { endEdges.add(e);
         * incidentEdges.put(end, endEdges); } else { return false; }
         * 
         * } else { this.addVertex(end); endEdges = incidentEdges.get(end);
         * endEdges.add(e); } if (sourceEdges != null) { if
         * (!sourceEdges.contains(e)) { sourceEdges.add(e);
         * incidentEdges.put(source, sourceEdges); } else { return false; } }
         * else { this.addVertex(source); sourceEdges =
         * incidentEdges.get(source); sourceEdges.add(e); } this.numEdges++;
         * return true;
         */
    }

    /**
     * Remove an edge if there.
     * 
     * @param v
     *            the starting vertex
     * @param u
     *            the ending vertex
     * @return true if delete, false if wasn't there
     */
    public boolean deleteEdge(GVertex<VT> v, GVertex<VT> u) {
        AbstractMap.SimpleEntry<GVertex<VT>, ArrayList<WEdge<VT>>> end = 
                this.incidentEdges.get(u.id());
        AbstractMap.SimpleEntry<GVertex<VT>, ArrayList<WEdge<VT>>> 
            source = this.incidentEdges.get(v.id());
        
        if (end == null || source == null) {
            return false;
        }
        
        ArrayList<WEdge<VT>> endEdges = end.getValue();
        ArrayList<WEdge<VT>> sourceEdges = source.getValue();
        boolean endContains = false;
        
        if (endEdges == null || sourceEdges == null) {
            return false;
        }
        
        
        
        
        return this.helperRemove(endContains, v, u, 
                sourceEdges, endEdges, end, source);
    }
    
    
    /** Helper method for removing edge.
     * 
     * @param endContains is a boolean telling 
     *         if end contains a vertex
     * @param v the vertex of edge to be removed
     * @param u the other vertex of the edge to be removed
     * @param sourceEdges is arraylist of source edges
     * @param endEdges is arraylist of end edges
     * @param end is Arraylist of incident edges to u
     * @param source arraylist of incident edges to v
     * @return if successful
     */
    private boolean helperRemove(boolean endContains, GVertex<VT> v, 
                GVertex<VT> u, ArrayList<WEdge<VT>> sourceEdges, 
                    ArrayList<WEdge<VT>> endEdges, 
                        AbstractMap.SimpleEntry<GVertex<VT>, 
                            ArrayList<WEdge<VT>>> end, 
                                AbstractMap.SimpleEntry<GVertex<VT>, 
                                    ArrayList<WEdge<VT>>> source) {
        
        Iterator<WEdge<VT>> sourceIter = sourceEdges.iterator();
        boolean sourceContains = false; 
        
        Iterator<WEdge<VT>> endIter = endEdges.iterator();
        WEdge<VT> remove = null;
        
        while (endIter.hasNext()) {
            WEdge<VT> ee = endIter.next();
            if (ee.source().equals(v)) {
                remove = ee;
                endContains = true;
            }
        }
        
        if (endContains) {
            endEdges.remove(remove);
        }
        
        
        while (sourceIter.hasNext()) {
            WEdge<VT> se = sourceIter.next();
            if (se.end().equals(u)) {
                remove = se;
                sourceContains = true;
            }
        }
        
        if (sourceContains) {
            sourceEdges.remove(remove);
        }
        
        end.setValue(endEdges);
        source.setValue(sourceEdges);
        this.incidentEdges.put(u.id(), end);
        this.incidentEdges.put(v.id(), source);
        
        if (sourceContains && endContains) {
            this.numEdges--;
        }
        
        return sourceContains && endContains;
        
    }

    /**
     * Return true if there is an edge between v and u.
     * 
     * @param v
     *            the starting vertex
     * @param u
     *            the ending vertex
     * @return true if there is an edge between them, false otherwise
     */
    public boolean areAdjacent(GVertex<VT> v, GVertex<VT> u) {
        AbstractMap.SimpleEntry<GVertex<VT>, ArrayList<WEdge<VT>>> end = 
                this.incidentEdges.get(u.id());
        AbstractMap.SimpleEntry<GVertex<VT>, ArrayList<WEdge<VT>>> source = 
                this.incidentEdges.get(v.id());
        
        if (end == null || source == null) {
            return false;
        }
        
        ArrayList<WEdge<VT>> endEdges = end.getValue();
        ArrayList<WEdge<VT>> sourceEdges = source.getValue();
        boolean endContains = false;
        if (endEdges == null || sourceEdges == null) {
            return false;
        }
        Iterator<WEdge<VT>> endIter = endEdges.iterator();
        while (endIter.hasNext()) {
            WEdge<VT> ee = endIter.next();
            if (ee.source().equals(v) || ee.source().equals(u)) {
                endContains = true;
            }
        }
        
        Iterator<WEdge<VT>> sourceIter = sourceEdges.iterator();
        
     
        return this.helperAreAdjacent(sourceIter, v, u, endContains);
    }
    
    /**
     * Helper method for areAdjacent function.
     * @param sourceIter is the iterator over source edges
     * @param v is first vertex being compared
     * @param u is second vertex being compared
     * @param endContains if there is an edge where end contains u or v
     * @return true if they are adjacent, else false
     */
    private boolean helperAreAdjacent(Iterator<WEdge<VT>> sourceIter, 
            GVertex<VT> v, GVertex<VT> u, boolean endContains) {
        
        boolean sourceContains = false;
        
        
        while (sourceIter.hasNext()) {
            WEdge<VT> se = sourceIter.next();
            if (se.end().equals(u) || se.end().equals(v)) {
                sourceContains = true;
            }
        }
        
        return endContains && sourceContains;
    }

    /**
     * Return a list of all the neighbors of vertex v.
     * 
     * @param v
     *            the vertex source
     * @return the neighboring vertices
     */
    public List<GVertex<VT>> neighbors(GVertex<VT> v) {
        AbstractMap.SimpleEntry<GVertex<VT>, ArrayList<WEdge<VT>>> vEntry 
            = this.incidentEdges.get(v.id());
        if (vEntry == null) {
            return new ArrayList<GVertex<VT>>();
        }
        ArrayList<WEdge<VT>> edges = vEntry.getValue();
        if (edges == null) {
            return new ArrayList<GVertex<VT>>();
        }
        List<GVertex<VT>> neighbors = new ArrayList<GVertex<VT>>();
        for (WEdge<VT> edge : edges) {
            GVertex<VT> source = edge.source();
            GVertex<VT> end = edge.end();
            if (!source.equals(v)) {
                neighbors.add(source);
            } else if (!end.equals(v)) {
                neighbors.add(end);
            }
        }
        return neighbors;
    }

    /**
     * Return the number of edges incident to v.
     * 
     * @param v
     *            the vertex source
     * @return the number of incident edges
     */
    public int degree(GVertex<VT> v) {
        AbstractMap.SimpleEntry<GVertex<VT>, ArrayList<WEdge<VT>>> vEntry 
            = this.incidentEdges.get(v.id());
        if (vEntry == null) {
            return -1;
        }
        ArrayList<WEdge<VT>> edges = vEntry.getValue();
        if (edges != null) {
            return edges.size();
        } else {
            System.err.println("Vertex does not exist in graph");
            return -1;
        }
    }

    /**
     * See if an edge and vertex are incident to each other.
     * 
     * @param e
     *            the edge
     * @param v
     *            the vertex to check
     * @return true if v is an endpoint of edge e
     */
    public boolean areIncident(WEdge<VT> e, GVertex<VT> v) {
        AbstractMap.SimpleEntry<GVertex<VT>, ArrayList<WEdge<VT>>> vEntry = 
                this.incidentEdges.get(v.id());
        ArrayList<WEdge<VT>> edges = vEntry.getValue();
        if (edges == null) {
            System.err.println("Vertex does not exist in graph");
            return false;
        }
        for (WEdge<VT> edge : edges) {
            if (edge.equals(e)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return a list of all the edges.
     * 
     * @return the list
     */
    public List<WEdge<VT>> allEdges() {
        HashSet<WEdge<VT>> uniqEdges = new HashSet<WEdge<VT>>();
        Iterator iter = this.incidentEdges.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<Integer, AbstractMap.SimpleEntry<GVertex<VT>, 
                ArrayList<WEdge<VT>>>> entry = (Map.Entry) iter.next();
            uniqEdges.addAll(entry.getValue().getValue());
        }
        List<WEdge<VT>> edges = new ArrayList<WEdge<VT>>();
        edges.addAll(uniqEdges);
        return edges;
    }

    /**
     * Return a list of all the vertices.
     * 
     * @return the list
     */
    public List<GVertex<VT>> allVertices() {
        Set<Map.Entry<Integer, AbstractMap.SimpleEntry<GVertex<VT>, 
            ArrayList<WEdge<VT>>>>> verts = this.incidentEdges.entrySet();
        
        Iterator iter = verts.iterator();
        
        List<GVertex<VT>> ret = new ArrayList<GVertex<VT>>();
        
        while (iter.hasNext()) {
            Map.Entry<Integer, AbstractMap.SimpleEntry<GVertex<VT>, 
                ArrayList<WEdge<VT>>>> entry = (Map.Entry) iter.next();
            
            ret.add(entry.getValue().getKey());
        }
        return ret;
    }

    /**
     * Return a list of all the vertices that can be reached from v, in the
     * order in which they would be visited in a depth-first search starting at
     * v.
     * 
     * @param v
     *            the starting vertex
     * @return the list of reachable vertices
     */
    public List<GVertex<VT>> depthFirst(GVertex<VT> v) {
        ArrayList<GVertex<VT>> reaches = new ArrayList<GVertex<VT>>();
        // using LinkedList<Vertex> as a Stack
        LinkedList<GVertex<VT>> stack = new LinkedList<GVertex<VT>>();
        if (this.numVerts() == 0 || !this.allVertices().contains(v)) {
            return reaches;
        }
        boolean[] visited = new boolean[this.numVerts()]; // inits to false
        stack.addFirst(v);
        visited[v.id()] = true;
        while (!stack.isEmpty()) {
            v = stack.removeFirst();
            reaches.add(v);
            for (GVertex<VT> u : this.neighbors(v)) {
                if (!visited[u.id()]) {
                    visited[u.id()] = true;
                    stack.addFirst(u);
                }
            }
        }
        return reaches;
    }

    /**
     * Return a list of all the edges incident on vertex v.
     * 
     * @param v
     *            the starting vertex
     * @return the incident edges
     */
    public List<WEdge<VT>> incidentEdges(GVertex<VT> v) {
        AbstractMap.SimpleEntry<GVertex<VT>,
            ArrayList<WEdge<VT>>> vEntry = this.incidentEdges.get(v.id());
        if (vEntry == null) {
            return new ArrayList<WEdge<VT>>();
        }
        ArrayList<WEdge<VT>> edges = vEntry.getValue();
        if (edges == null) {
            return new ArrayList<WEdge<VT>>();
        }
        List<WEdge<VT>> ret = new ArrayList<WEdge<VT>>(edges);
        return ret;
    }

    /**
     * Return a list of edges in a minimum spanning forest by implementing
     * Kruskal's algorithm using fast union/finds.
     * 
     * @return a list of the edges in the minimum spanning forest
     */
    public List<WEdge<VT>> kruskals() {
        Partition part = new Partition(this.numVerts());
        PQHeap<WEdge<VT>> heap = new PQHeap<WEdge<VT>>(this.numEdges());
        /* Minimum spanning tree */
        ArrayList<WEdge<VT>> mst = new ArrayList<WEdge<VT>>();

        // System.out.println("Edges: " + this.allEdges());
        heap.init(this.allEdges());
        // System.out.println("Heap: " + heap.toString());
        while (!heap.isEmpty()) {
            /* remove min edge from heap */
            WEdge<VT> minEdge = heap.remove();

            /* source vertex of minEdge */
            GVertex<VT> sourceVertex = minEdge.source();
            /* end vertex of minEdge */
            GVertex<VT> endVertex = minEdge.end();

            /*
             * Union sets and add edge to mst if parent of source and end arent
             * the same.
             */
            if (part.find(endVertex.id()) != part.find(sourceVertex.id())) {
                part.union(endVertex.id(), sourceVertex.id());
                mst.add(minEdge);
            }
        }

        return mst;
    }
}
