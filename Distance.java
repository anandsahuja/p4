/** CS.226.02 Project #4
 * Anand Ahuja | aahuja10
 * Jacob Lin   | ylin82
 * Ryan Dens   | rdens1
 */


/**Public Interface for Distance used for weights.
 * @param <T>
 */

public interface Distance<T> {
    /** Distance between data elements 1 and 2.
     * 
     *  @param one element 1
     *  @param two element 2
     *  @return double distance
     */
    double distance(T one, T two);
}